class NotAuthenticatedError {
   constructor(message) {
      this.name = 'NotAuthenticatedError';
      this.message = message;
      this.status = 401;
   }
}

module.exports = {
   NotAuthenticatedError
};