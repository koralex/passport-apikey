const { Strategy: PassportStrategy } = require('passport-strategy');
const { ExtractJwt } = require('passport-jwt');
const { get } = require('lodash');
const { NotAuthenticatedError } = require('./errors');

class Strategy extends PassportStrategy {
   constructor({queryAttributeName, headerName, headerPrefix, bodyAttribute}, passReqToCallback, verify) {
      super();
      this.apiKeyHeader = {header: headerName || 'X-Api-Key', headerPrefix: 'Bearer'};
      this.apiKeyHeader.header = this.apiKeyHeader.header.toLowerCase();
      this.queryAttributeName = queryAttributeName || 'api_key';
      this.bodyAttributeName = bodyAttribute || 'api_key';

      this.name = 'apikey';
      this.verify = verify;
      this.passReqToCallback = passReqToCallback || false;
   }

   extractKey(req){
      const extractor = ExtractJwt.fromExtractors([ // a passport-jwt option determining where to parse the JWT
         function getJWTFromRequest(request) {
            const apiKey = get(request, `params.query.${this.queryAttributeName}`, null);

            if (apiKey !== null) {
               // Remove the api_key from the params to avoid any filtering running on it
               delete request.params.query[this.queryAttributeName];
            }

            return apiKey;
         },
         ExtractJwt.fromAuthHeaderWithScheme(this.apiKeyHeader.headerPrefix), // Allowing "Bearer" prefix
         ExtractJwt.fromHeader(this.apiKeyHeader.header), // From "Authorization" header
         ExtractJwt.fromBodyField(this.bodyAttributeName), // From body
      ]);

      const _extractor = extractor.bind(this);

      return _extractor(req);
   }

   authenticate(req) {
      const apiKey = this.extractKey(req);

      if (!apiKey) {
         return this.fail(new NotAuthenticatedError('Missing API Key'), 401);
      }

      let verified = (err, user, info) => {
         if (err) {
            return this.error(err);
         }

         if (!user) {
            return this.fail(info, 401);
         }

         this.success(user, info);
      };

      this.verify(apiKey, verified, this.passReqToCallback ? req : undefined);
   }
}

module.exports = {
   Strategy
};
